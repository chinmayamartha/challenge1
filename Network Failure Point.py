# Time Complexity is O(V+E)

from collections import defaultdict

class Graph:
    
    def __init__(self,v):
        self.graph_outbound_dict=defaultdict(list)
        self.graph_inbound_dict=defaultdict(list)
        self.v=v
        
    """Method to add edge to inbound and outbound dict
       Param:a,b(vertices)
    """
    def addEdge(self,a,b):
        self.graph_outbound_dict[a].append(b)
        self.graph_inbound_dict[b].append(a)
        
    """Method to find node with max connections
       Return list of nodes with max connections
    """
    def identify_router(self):
        max=0
        list_max_node=[]
        for k in sorted(self.graph_inbound_dict):
            if(k in (self.graph_outbound_dict.keys())):
                temp=len(self.graph_outbound_dict[k]) + len(self.graph_inbound_dict[k])
                if (temp == max):
                    max = temp
                    list_max_node.append(k)
                if(temp > max):
                    max=temp
                    list_max_node=[]
                    list_max_node.append(k)
        return list_max_node
    
if __name__ =="__main__":
    l=input().rstrip().split(' -> ')
    s=set(l)
    g=Graph(len(s))
    for i in range(1,len(l)):
        g.addEdge(l[i-1],l[i])
    result=g.identify_router()
    if(len(result) > 1):
        print(','.join(result))
    else:
        print(result[0])
