#Time complexity of the below solution is O(n)

"""Method to compress the string
   Params-> s:string
   Return string
"""

def string_compression(s):
    o_string=''
    d={}
    c=1
    for i in range(1,len(s)):
        if(s[i]==s[i-1]):
            c+=1

        else:
            o_string+=s[i-1]+str(c)
            c=1

    o_string+=s[len(s)-1]+str(c)
    for i in range(len(o_string)):
        if((i%2) != 0):
            if(int(o_string[i]) > 1):
                return o_string

    return s

if(__name__=='__main__'):
    s=input()
    result=string_compression(s)
    print(result)